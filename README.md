# Blender 3D Shader and Geometry Nodes
### Blender Geometry and Shader Nodes

### Three Shader Nodes featured in the **Kinetic Kudu** 22.10 edition of the Ubuntu Wallpaper Competition:

| Parameter | Nodes    | Description                |
| :-------- | :------- | :------------------------- |
| `Fosfenos`| `Shader Nodes` | `Ubuntu Wallpaper Competition`|
| `Kear`    | `Shader Nodes` | `Ubuntu Wallpaper Competition`|
| `Kubex`   | `Shader Nodes` | `Ubuntu Wallpaper Competition`|

Shader Editor Created with [Blender](https://www.blender.org/) 3.5.0 CC BY-SA 4.0

## Blender Tips

- Blender add Texture Coordinate and Mapping

```bash
  Node + Ctrl + T
```

- Blender Material Output

```bash
  Ctrl + Shift + Object
```

- The new Mix Node in Blender for **Mix Color**

```bash
  Add + Color + Mix Color
```
# Introduction to Using the Terminal with Blender on Linux

**Blender** is a powerful free and open-source tool for creating 3D content. While most users interact with Blender through its graphical interface, it is also possible to use the terminal to automate tasks, optimize workflows, and run processes in the background. This is especially useful on Linux systems, where the terminal is a fundamental tool.

In this introduction, we will explore how to launch Blender from the terminal, basic commands, how to execute scripts, and advanced configurations to make the most of its functionality.

# Basic Options and Commands

**Blender** supports a variety of options that you can pass as arguments in the terminal. Some of the most common include:

- Open a .blend file:
```bash
  blender file.blend
```

- Render an image from a .blend file:
```bash
  blender file.blend -b -f 1
```

-b: Runs Blender in background mode, which disables the graphical user interface.

-f 1: Renders frame 1. Replace 1 with the frame number you want to render.

### **Output location:** 
- By default, the rendered image is saved to the output path specified in the .blend file's render settings. You can customize the output path with the -o option:
```bash
  blender file.blend -b -o //output_folder/filename -f 1
```
-o: Specifies the output folder and filename. Use // to indicate a relative path from the .blend file's location.

### **Example command:** 
- If your .blend file is at /home/user/project.blend and you want to save the rendered frame as output.png in /home/user/renders, use:
```bash
  blender /home/user/project.blend -b -o /home/user/renders/output -f 1
```
### Basic Command to Render an Animation:
```bash
  ./blender -b /path/to/file/animation.blend -a
```
-s: Starting frame.

-e: Ending frame.

### Change the Frame Range:
- To override the start and end frames set in the .blend file:
```bash
  blender file.blend -b -s 1 -e 250 -a
```
-s: Start frame (e.g., frame 1).

-e: End frame (e.g., frame 250).

### Log the Output:
- To save the terminal logs to a file for later review:
```bash
  blender file.blend -b -a > render_log.txt
```

### Complete Example
- Render an animation using Cycles from frame 1 to 250, as an MP4 video, using 8 threads:
```bash
  blender -b /home/user/projects/animation.blend -o /home/user/output/ -F FFmpeg -E CYCLES -s 1 -e 250 -t 8 -a
```
### Blender Uses the Render Settings Saved in the .blend File:
- Dimensions.
- Render engine.
- Frame range.
- Output format.
- Sample configuration.
- Lighting and material settings.

## Running Python Scripts

One of the most advanced and powerful uses of the terminal in Blender is the ability to run Python scripts to automate tasks. This is done with the --python option:

- Run a script directly:
```bash
  blender --python script.py
```
- Run a script within a .blend file:
```bash
  blender file.blend --python script.py
```
- Pass arguments to the script:
```bash
  blender --python script.py -- --arg1 value1
```
Note: Arguments after -- are passed to the script and not to Blender.

## Get the Number of Frames in a .blend File
In Linux, there is no native command to directly retrieve the number of frames from a .blend file via the terminal. However, you can use Blender in terminal mode to extract this information.

- The following command allows you to run Blender in background mode to list the settings of the .blend file, including the start and end frames:
```bash
  ./blender -b /path/to/file.blend --python-expr "import bpy; print(f'Frames: {bpy.context.scene.frame_start} to {bpy.context.scene.frame_end}')"
```
-b: Runs Blender in background mode (no graphical interface).

--python-expr: Executes a Python code snippet to retrieve information from the file.

bpy.context.scene.frame_start: Displays the starting frame.

bpy.context.scene.frame_end: Displays the ending frame.

### Example Output
- If the frame range in the .blend file is from 1 to 250, the command will print:
```bash
Frames: 1 to 250
```
### Advanced Debugging Options
For debugging or analyzing performance:
- Verbose Logging:
```bash
blender -d
```
- Factory Settings: Reset Blender to its factory settings:
```bash
blender --factory-startup
```
### Conclusion on the Advantages of Using the Terminal in Blender with Linux
- Using the terminal in Blender on Linux provides a powerful set of tools that significantly enhance the software's capabilities. Key advantages include task automation for repetitive processes, such as rendering multiple files or animation sequences, and resource optimization by running processes in the background while minimizing the use of the graphical interface.

- The terminal also facilitates integration with custom Python scripts, enabling the automation of complex workflows and tailoring Blender to specific needs. It becomes an essential tool in professional environments where batch rendering management, configuration customization, and remote execution on servers are crucial for maximizing productivity.

- Finally, working with the terminal fosters a deeper understanding of Blender's technical capabilities, promoting the development of advanced skills and fully leveraging this powerful software on Linux systems. Thus, the terminal not only optimizes time and resources but also stands as an indispensable tool for advanced and professional users.






